# echo "Setting CALOCalib v1r0 in /afs/cern.ch/user/m/mfontana/DaVinciDev_v43r1/WG"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/lhcb.cern.ch/lib/contrib/CMT/v1r20p20090520
endif
source ${CMTROOT}/mgr/setup.csh

set tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set tempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt setup -csh -pack=CALOCalib -version=v1r0 -path=/afs/cern.ch/user/m/mfontana/DaVinciDev_v43r1/WG  -no_cleanup $* >${tempfile}; source ${tempfile}
/bin/rm -f ${tempfile}

