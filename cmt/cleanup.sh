if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/lhcb.cern.ch/lib/contrib/CMT/v1r20p20090520; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then tempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt cleanup -sh -pack=CALOCalib -version=v1r0 -path=/afs/cern.ch/user/m/mfontana/DaVinciDev_v43r1/WG $* >${tempfile}; . ${tempfile}
/bin/rm -f ${tempfile}

