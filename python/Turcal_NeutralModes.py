from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
from CommonParticles.Utils import *
from DecayTreeTuple.Configuration import *
from Configurables import PrintDecayTree,  DecayTreeTuple, DaVinci, SelDSTWriter, FilterDesktop, FilterInTrees, LoKi__HDRFilter, FilterDesktop,CombineParticles
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from Configurables import TupleToolRecoStats,TupleToolProtoPData, TupleToolCaloHypo
from Configurables import TupleToolKinematic, TupleToolTrackInfo, TupleToolPrimaries
from Configurables import TupleToolCaloHypo, TupleToolGeometry, TupleToolRecoStats, TupleToolPid



DaVinci().Simulation = 0
DaVinci().EvtMax = -1
DaVinci().DataType = "2017"
DaVinci().TupleFile = "CALOCALIB.ROOT"
DaVinci().InputType = "DST"
DaVinci().Lumi = True

#last runs of 2017 (for script testing: lb-run DaVinci/v42r5 gaudirun.py this_script.py)
#DaVinci().Input = ['root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/buffer/lhcb/LHCb/Collision17/FULLTURBO.DST/00067725/0000/00067725_00006461_2.fullturbo.dst']







#-------------------- Eta -> Mu Mu Gamma -------------------------------

#Combiners##############################################################
MyEta2MuMuGamma = CombineParticles("MyEta2MuMuGamma",
                                   DecayDescriptor = "eta -> mu+ mu- gamma",
                                   Inputs = [ "Phys/StdLooseMuons/Particles" ,"Phys/StdLooseAllPhotons/Particles"],
                                   DaughtersCuts = { "gamma" : "(PT > 500*MeV)" , "mu+" : "(PT > 500.0*MeV) & (PROBNNmu > 0.95) & (MIPCHI2DV(PRIMARY) < 6.0)"},
                                   CombinationCut = "(ADAMASS('eta')<200*MeV)",
                                   MotherCut = "(MM>400) & (M<700)")

#Ntuple##################################################################
MyEta2MuMuGamma_Tuple = DecayTreeTuple("MyEta2MuMuGamma_Tuple")
MyEta2MuMuGamma_Tuple.Inputs = ["Phys/MyEta2MuMuGamma/Particles"]
MyEta2MuMuGamma_Tuple.Decay = "[eta -> ^mu+ ^mu- ^gamma]CC"
MyEta2MuMuGamma_Tuple.ToolList = ["TupleToolEventInfo","TupleToolRecoStats","TupleToolKinematic"]

MyEta2MuMuGamma_Tuple.addBranches({
    "eta"         : "[eta]CC",
    "mu_plus"     : "[eta -> ^mu+  mu-  gamma ]CC",
    "mu_minus"    : "[eta ->  mu+ ^mu-  gamma ]CC",
    "gamma"       : "[eta ->  mu+  mu- ^gamma ]CC"})

#eta
MyEta2MuMuGamma_Tuple.eta.addTupleTool("TupleToolGeometry")
MyEta2MuMuGamma_Tuple.eta.addTupleTool("TupleToolVtxIsoln")

#gamma
MyEta2MuMuGamma_Tuple.gamma.addTupleTool("TupleToolConeIsolation")
MyEta2MuMuGamma_Tuple.gamma.addTupleTool("TupleToolPhotonInfo")
MyEta2MuMuGamma_Tuple.gamma.addTupleTool("TupleToolProtoPData")
MyEta2MuMuGamma_Tuple.gamma.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID"]

MyEta2MuMuGamma_Tuple.gamma.addTupleTool("TupleToolCaloHypo")
MyEta2MuMuGamma_Tuple.gamma.TupleToolCaloHypo.DataList = ["HypoSpdM", "ToSpdM" , "HypoPrsM" , "ToPrsM" , "CaloHypoPileUpEnergy" ]

#vetoes
MyEta2MuMuGamma_Tuple.addTupleTool("TupleToolVeto")
MyEta2MuMuGamma_Tuple.TupleToolVeto.Particle="gamma"
MyEta2MuMuGamma_Tuple.TupleToolVeto.Veto["Pi0R"]=["/Event/Phys/StdLooseResolvedPi0/Particles"]
MyEta2MuMuGamma_Tuple.TupleToolVeto.Veto["EtaR"]=["/Event/Phys/StdLooseResolvedEta/Particles"]

#muons
LoKi_mu_plus = MyEta2MuMuGamma_Tuple.mu_plus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_mu_plus")
LoKi_mu_minus = MyEta2MuMuGamma_Tuple.mu_minus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_mu_minus")
LoKi_mu_plus.Variables = {"PROBNNmu" : "PROBNNmu" , "MIPCHI2DV"  : "MIPCHI2DV(PRIMARY)" }
LoKi_mu_minus.Variables = {"PROBNNmu" : "PROBNNmu" , "MIPCHI2DV"  : "MIPCHI2DV(PRIMARY)" }

#------------------------------------------------------------------


#-------------------- B+ -> V gamma ------------------------------------

#Combiners##############################################################
MyKstr2Kpi = CombineParticles("MyKstr2Kpi",
                              DecayDescriptor = "[K*(892)0 -> K+ pi-]cc",                            
                              Inputs = [ "Phys/StdLoosePions/Particles" ,"Phys/StdLooseKaons/Particles" ],
                              DaughtersCuts = { "pi-" : "(PT > 500*MeV) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 55) & (PIDK < 0)",
                                                "K+"  : "(PT > 500*MeV) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 55) & (PIDK > 0)" },
                              CombinationCut = "(ADAMASS('K*(892)0')<100)",
                              MotherCut = "(VFASPF(VCHI2PDOF)<9)" )

MyB02KstrGamma = CombineParticles("MyB02KstrGamma",
                                  DecayDescriptor= "[B0 -> K*(892)0 gamma]cc",
                                  Inputs = ["Phys/MyKstr2Kpi" , "Phys/StdLooseAllPhotons"],
                                  DaughtersCuts = { "gamma" : "(PT>2600*MeV)" },
                                  CombinationCut = "(ADAMASS('B0') < 1600)",
                                  MotherCut = "(acos(BPVDIRA) < 0.02) & (BPVIPCHI2() < 15) & (PT > 3000.0*MeV) & (ADMASS('B0') < 1500) & (BPVVDCHI2 > 0)")

MyPhi2KK = CombineParticles("MyPhi2KK",
                            DecayDescriptor = "[phi(1020) -> K+ K-]cc",                            
                            Inputs = [ "Phys/StdLoosePions/Particles" ,"Phys/StdLooseKaons/Particles" ],
                            DaughtersCuts = { "K+" : "(PT > 500*MeV) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 55) & (PIDK > 0)"},
                            CombinationCut = "(ADAMASS('phi(1020)')<30)",
                            MotherCut = "(VFASPF(VCHI2PDOF)<9)" )

MyBs2PhiGamma = CombineParticles("MyBs2PhiGamma",
                                  DecayDescriptor= "[B_s0 -> phi(1020) gamma]cc",
                                  Inputs = ["Phys/MyPhi2KK" , "Phys/StdLooseAllPhotons"],
                                  DaughtersCuts = { "gamma" : "(PT>2600*MeV)" },
                                  CombinationCut = "(ADAMASS('B_s0') < 1600)",
                                  MotherCut = "(acos(BPVDIRA) < 0.02) & (BPVIPCHI2() < 15) & (PT > 3000.0*MeV) & (ADMASS('B_s0') < 1500) & (BPVVDCHI2 > 0)")

#Ntuple##################################################################
#B0 -> K* gamma
MyB02KstrGamma_Tuple = DecayTreeTuple("MyB02KstrGamma_Tuple")
MyB02KstrGamma_Tuple.Decay = "[B0 -> ^(K*(892)0 -> ^K+ ^pi-) ^gamma]CC"
MyB02KstrGamma_Tuple.Inputs = [ "Phys/MyB02KstrGamma/Particles" ]
MyB02KstrGamma_Tuple.ToolList = ["TupleToolEventInfo","TupleToolRecoStats","TupleToolKinematic"]

MyB02KstrGamma_Tuple.addBranches({
    "B0"         : "[B0]CC",
    "Kstr"       : "[B0 -> ^(K*(892)0 ->  K+  pi-)  gamma ]CC",
    "Kplus"      : "[B0 -> (K*(892)0 ->  ^K+  pi-)  gamma ]CC",
    "pi_minus"   : "[B0 -> (K*(892)0 ->  K+  ^pi-)  gamma ]CC",
    "gamma"      : "[B0 -> (K*(892)0 ->  K+  pi-)  ^gamma ]CC"})

MyB02KstrGamma_Tuple.B0.addTupleTool("TupleToolGeometry")

MyB02KstrGamma_Tuple.Kstr.addTupleTool("TupleToolGeometry")
MyB02KstrGamma_Tuple.Kstr.addTupleTool("TupleToolVtxIsoln")

MyB02KstrGamma_Tuple.Kplus.addTupleTool("TupleToolPid")
MyB02KstrGamma_Tuple.pi_minus.addTupleTool("TupleToolPid")

MyB02KstrGamma_Tuple.gamma.addTupleTool("TupleToolConeIsolation")
MyB02KstrGamma_Tuple.gamma.addTupleTool("TupleToolPhotonInfo")
MyB02KstrGamma_Tuple.gamma.addTupleTool("TupleToolProtoPData")
MyB02KstrGamma_Tuple.gamma.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID"]

MyB02KstrGamma_Tuple.gamma.addTupleTool("TupleToolCaloHypo")
MyB02KstrGamma_Tuple.gamma.TupleToolCaloHypo.DataList = ["HypoSpdM", "ToSpdM" , "HypoPrsM" , "ToPrsM" , "CaloHypoPileUpEnergy" ]

MyB02KstrGamma_Tuple.addTupleTool("TupleToolVeto")
MyB02KstrGamma_Tuple.TupleToolVeto.Particle="gamma"
MyB02KstrGamma_Tuple.TupleToolVeto.Veto["Pi0R"]=["/Event/Phys/StdLooseResolvedPi0/Particles"]
MyB02KstrGamma_Tuple.TupleToolVeto.Veto["EtaR"]=["/Event/Phys/StdLooseResolvedEta/Particles"]

LoKi_gamma = MyB02KstrGamma_Tuple.gamma.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_gamma")
LoKi_gamma.Variables = {"ETA" : "ETA" , "PHI"  : "PHI"}

MyBs2PhiGamma_Tuple = DecayTreeTuple("MyBs2PhiGamma_Tuple")
MyBs2PhiGamma_Tuple.Decay = "[B_s0 -> ^(phi(1020) -> ^K+ ^K-) ^gamma]CC"
MyBs2PhiGamma_Tuple.Inputs = [ "Phys/MyBs2PhiGamma/Particles" ]
MyBs2PhiGamma_Tuple.ToolList = ["TupleToolEventInfo","TupleToolRecoStats","TupleToolKinematic"]

#Bs -> Phi gamma
MyBs2PhiGamma_Tuple.addBranches({
    "B_s0"       : "[B_s0]CC",
    "phi"        : "[B_s0 -> ^(phi(1020) ->  K+  K-)  gamma ]CC",
    "Kplus"      : "[B_s0 -> (phi(1020) ->  ^K+  K-)  gamma ]CC",
    "Kminus"     : "[B_s0 -> (phi(1020) ->  K+  ^K-)  gamma ]CC",
    "gamma"      : "[B_s0 -> (phi(1020) ->  K+  K-)  ^gamma ]CC"})

MyBs2PhiGamma_Tuple.B_s0.addTupleTool("TupleToolGeometry")

MyBs2PhiGamma_Tuple.phi.addTupleTool("TupleToolGeometry")
MyBs2PhiGamma_Tuple.phi.addTupleTool("TupleToolVtxIsoln")

MyBs2PhiGamma_Tuple.Kplus.addTupleTool("TupleToolPid")
MyBs2PhiGamma_Tuple.Kminus.addTupleTool("TupleToolPid")

MyBs2PhiGamma_Tuple.gamma.addTupleTool("TupleToolConeIsolation")
MyBs2PhiGamma_Tuple.gamma.addTupleTool("TupleToolPhotonInfo")
MyBs2PhiGamma_Tuple.gamma.addTupleTool("TupleToolProtoPData")
MyBs2PhiGamma_Tuple.gamma.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID"]

MyBs2PhiGamma_Tuple.gamma.addTupleTool("TupleToolCaloHypo")
MyBs2PhiGamma_Tuple.gamma.TupleToolCaloHypo.DataList = ["HypoSpdM", "ToSpdM" , "HypoPrsM" , "ToPrsM" , "CaloHypoPileUpEnergy" ]

MyBs2PhiGamma_Tuple.addTupleTool("TupleToolVeto")
MyBs2PhiGamma_Tuple.TupleToolVeto.Particle="gamma"
MyBs2PhiGamma_Tuple.TupleToolVeto.Veto["Pi0R"]=["/Event/Phys/StdLooseResolvedPi0/Particles"]
MyBs2PhiGamma_Tuple.TupleToolVeto.Veto["EtaR"]=["/Event/Phys/StdLooseResolvedEta/Particles"]

LoKi_gamma = MyBs2PhiGamma_Tuple.gamma.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_gamma")
LoKi_gamma.Variables = {"ETA" : "ETA" , "PHI"  : "PHI"}

#------------------------------------------------------------------


#-------------------- D+ -> eta' pi ------------------------------------

#Combiners##############################################################
MyEtap2PiPiGamma = CombineParticles("MyEtap2PiPiGamma",
                                    DecayDescriptor = "[eta_prime -> pi+ pi- gamma]cc",
                                    Inputs = ["Phys/StdAllNoPIDsPions/Particles","Phys/StdLooseAllPhotons/Particles"],
                                    DaughtersCuts = {'pi+' : '(PT > 500.0) & (P > 1000.0) & (MIPCHI2DV(PRIMARY) > 16.0) & (TRCHI2DOF < 5) & (TRGHOSTPROB < 0.5) & (in_range(1000.0, P, 100000.0)) & (PIDK-PIDpi < 0)' , 'gamma' : '(PT > 1000.0)'},
                                    CombinationCut = "in_range( 900.0,AM,1020.0 )",
                                    MotherCut = "ALL")

MyDs2EtapPi = CombineParticles("MyDs2EtapPi",
                               DecayDescriptor = "[D_s+ -> eta_prime pi+]cc",
                               Inputs = ["Phys/MyEtap2PiPiGamma/Particles","Phys/StdAllNoPIDsPions/Particles"],
                               DaughtersCuts = {'pi+' : '(PT > 600.0)& (P > 1000.0) & (MIPCHI2DV(PRIMARY) > 16.0)& (TRCHI2DOF < 5) & (TRGHOSTPROB < 0.5) '},
                               CombinationCut = "(APT > 2000.0)& ( in_range( 1700.0,AM,2200.0) )",
                               MotherCut = "(VFASPF(VCHI2PDOF) < 4) & (BPVLTIME() > 0.00025) & (BPVDIRA>0.999975) & (BPVIP()<0.05) & (BPVIPCHI2()<10)")

#Ntuple##################################################################
MyDs2EtapPi_Tuple = DecayTreeTuple("MyDs2EtapPi_Tuple")
MyDs2EtapPi_Tuple.Decay = '[D_s+ -> ^(eta_prime -> ^pi+ ^pi- ^gamma) ^pi+]CC'
MyDs2EtapPi_Tuple.Inputs = ["Phys/MyDs2EtapPi"]
MyDs2EtapPi_Tuple.ToolList = ["TupleToolEventInfo","TupleToolRecoStats","TupleToolKinematic"]

#Branches
MyDs2EtapPi_Tuple.addBranches({
    "D_s_plus"     : "[D_s+]CC",
    "eta_prime"  : "[D_s+ -> pi+ ^(eta_prime -> pi+ pi- gamma) ]CC",
    "pi_plus"    : "[D_s+ -> pi+ (eta_prime -> ^pi+ pi- gamma) ]CC",
    "pi_minus"   : "[D_s+ -> pi+ (eta_prime -> pi+ ^pi- gamma) ]CC",
    "gamma"      : "[D_s+ -> pi+ (eta_prime -> pi+ pi- ^gamma) ]CC",
    "pi_bach"    : "[D_s+ -> ^pi+ (eta_prime -> pi+ pi- gamma) ]CC"})

#D+
MyDs2EtapPi_Tuple.D_s_plus.addTupleTool("TupleToolGeometry")
MyDs2EtapPi_Tuple.D_s_plus.addTupleTool("TupleToolVtxIsoln")

MyDs2EtapPi_Tuple.D_s_plus.addTupleTool("TupleToolDecayTreeFitter/DTF")
MyDs2EtapPi_Tuple.D_s_plus.DTF.constrainToOriginVertex = False
MyDs2EtapPi_Tuple.D_s_plus.DTF.Verbose = False
MyDs2EtapPi_Tuple.D_s_plus.DTF.daughtersToConstrain = ['eta_prime']
MyDs2EtapPi_Tuple.D_s_plus.DTF.UpdateDaughters = False

#eta'
LoKi_eta_prime = MyDs2EtapPi_Tuple.eta_prime.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_eta_prime")
LoKi_eta_prime.Preambulo = ["from LoKiCore.math import sqrt","PiPi_MM = sqrt(CHILD(MM,1)*CHILD(MM,1) + CHILD(MM,2)*CHILD(MM,2) + 2 *( CHILD(E,1)*CHILD(E,2) - (CHILD(PX,1)*CHILD(PX,2) + CHILD(PY,1)*CHILD(PY,2) + CHILD(PZ,1)*CHILD(PZ,2))))"]
LoKi_eta_prime.Variables = { "PiPi_MM" : "PiPi_MM" }

MyDs2EtapPi_Tuple.gamma.addTupleTool("TupleToolConeIsolation")
MyDs2EtapPi_Tuple.gamma.addTupleTool("TupleToolPhotonInfo")
MyDs2EtapPi_Tuple.gamma.addTupleTool("TupleToolProtoPData")
MyDs2EtapPi_Tuple.gamma.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID"]

MyDs2EtapPi_Tuple.gamma.addTupleTool("TupleToolCaloHypo")
MyDs2EtapPi_Tuple.gamma.TupleToolCaloHypo.DataList = ["HypoSpdM", "ToSpdM" , "HypoPrsM" , "ToPrsM" , "CaloHypoPileUpEnergy" ]

MyDs2EtapPi_Tuple.addTupleTool("TupleToolVeto")
MyDs2EtapPi_Tuple.TupleToolVeto.Particle="gamma"
MyDs2EtapPi_Tuple.TupleToolVeto.Veto["Pi0R"]=["/Event/Phys/StdLooseResolvedPi0/Particles"]
MyDs2EtapPi_Tuple.TupleToolVeto.Veto["EtaR"]=["/Event/Phys/StdLooseResolvedEta/Particles"]

MyDs2EtapPi_Tuple.pi_plus.addTupleTool("TupleToolPid")
MyDs2EtapPi_Tuple.pi_minus.addTupleTool("TupleToolPid")
MyDs2EtapPi_Tuple.pi_bach.addTupleTool("TupleToolPid")

#------------------------------------------------------------------


#-------------------- Ds* -> Ds gamma ----------------------------------

#Get Ds
StripDs2KKpi = [DataOnDemand("Phys/StdLooseDplus2KKPi/Particles")]

#Additional cuts (as in stripping 28)
D2hhhFTCalib_Filter = FilterInTrees ('D2hhhFTCalib_Filter'  ,Code = "( (MAXTREE('pi+'==ABSID, PIDK-PIDpi) < 3.0) & (MINTREE('K-'==ABSID, PIDK-PIDpi) > 7.0 ) & (MINTREE('K-'==ABSID, PROBNNk) > 0.1 ) & ((SUMTREE( ISBASIC , PT ) > 3200.0*MeV) & (2 <= NINGENERATION((MIPCHI2DV(PRIMARY) > 10.0 ) , 1))) & (PT > 2000.0) & (VFASPF(VCHI2/VDOF) < 6.0) & (BPVDIRA > 0.99995) & (BPVIPCHI2() < 1500.0) & (VFASPF(VMINVDCHI2DV(PRIMARY)) > 150.0) & (BPVLTIME() > 0.2*picosecond) & (DOCACHI2MAX < 50.0) & (in_range ( 1920.0 , M , 2040.0 )) )")
Ds2KKpi_Sel = Selection('Ds2KKpi_Sel',Algorithm = D2hhhFTCalib_Filter,RequiredSelections = StripDs2KKpi)
Ds2KKpi_Seq = SelectionSequence('Ds2EtapPi_Seq',TopSelection = Ds2KKpi_Sel)


#Combiners##############################################################
MyDsstr2DsGamma = CombineParticles("MyDsstr2DsGamma",
                                   DecayDescriptor= "[D*_s+ -> D+ gamma]cc",
                                   Inputs = [Ds2KKpi_Seq.outputLocation() , "Phys/StdLooseAllPhotons/Particles"],
                                   DaughtersCuts = { "gamma" : "(PT>500*MeV)" , "D+" : "(ADMASS('D_s+')<30*MeV)"},
                                   CombinationCut = "(AM>2000) & (AM<2300)",
                                   MotherCut="(BPVDIRA>0.999975) & (BPVIP()<0.05) & (BPVIPCHI2()<10) & (MM>2050) & (MM<2250)")

#Ntuple##################################################################
MyDsstr2DsGamma_Tuple = DecayTreeTuple("MyDsstr2DsGamma_Tuple")
MyDsstr2DsGamma_Tuple.Decay = "[D*_s+ -> ^D+ ^gamma]CC"
MyDsstr2DsGamma_Tuple.Inputs = [ "Phys/MyDsstr2DsGamma" ]
MyDsstr2DsGamma_Tuple.ToolList = ["TupleToolEventInfo","TupleToolKinematic","TupleToolRecoStats"]

MyDsstr2DsGamma_Tuple.addBranches({
    "D_s_str_plus"   : "[D*_s+]CC",
    "D_s_plus"      : "[D*_s+ -> ^D+ gamma ]CC",
    "gamma"         : "[D*_s+ -> D+ ^gamma ]CC"})

MyDsstr2DsGamma_Tuple.D_s_str_plus.addTupleTool("TupleToolGeometry")

MyDsstr2DsGamma_Tuple.D_s_plus.addTupleTool("TupleToolGeometry")
MyDsstr2DsGamma_Tuple.D_s_plus.addTupleTool("TupleToolVtxIsoln")

MyDsstr2DsGamma_Tuple.gamma.addTupleTool("TupleToolConeIsolation")
MyDsstr2DsGamma_Tuple.gamma.addTupleTool("TupleToolPhotonInfo")
MyDsstr2DsGamma_Tuple.gamma.addTupleTool("TupleToolProtoPData")
MyDsstr2DsGamma_Tuple.gamma.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID"]

MyDsstr2DsGamma_Tuple.gamma.addTupleTool("TupleToolCaloHypo")
MyDsstr2DsGamma_Tuple.gamma.TupleToolCaloHypo.DataList = ["HypoSpdM", "ToSpdM" , "HypoPrsM" , "ToPrsM" , "CaloHypoPileUpEnergy" ]

MyDsstr2DsGamma_Tuple.addTupleTool("TupleToolVeto")
MyDsstr2DsGamma_Tuple.TupleToolVeto.Particle="gamma"
MyDsstr2DsGamma_Tuple.TupleToolVeto.Veto["Pi0R"]=["/Event/Phys/StdLooseResolvedPi0/Particles"]
MyDsstr2DsGamma_Tuple.TupleToolVeto.Veto["EtaR"]=["/Event/Phys/StdLooseResolvedEta/Particles"]

LoKi_D_s_plus = MyDsstr2DsGamma_Tuple.D_s_plus.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_D_s_plus")
LoKi_gamma = MyDsstr2DsGamma_Tuple.gamma.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_gamma")

LoKi_D_s_plus.Variables = {"ETA" : "ETA" , "PHI"  : "PHI"}
LoKi_gamma.Variables = {"ETA" : "ETA" , "PHI"  : "PHI"}

#------------------------------------------------------------------


#-------------------- D* -> (D0 ->  K pi pi0) pi+ ----------------------

#Combiners##############################################################

#track filter + D0 combiner
MyD02Kpipi0R = CombineParticles("MyD02Kpipi0R",
                                DecayDescriptor = "[D0 -> K- pi+ pi0]cc",                            
                                Inputs = [ "Phys/StdLoosePions/Particles" ,"Phys/StdLooseKaons/Particles" , "Phys/StdLooseResolvedPi0"],
                                DaughtersCuts = { "pi+" : "(PT > 600*MeV) & (TRCHI2DOF < 5) & (TRGHOSTPROB < 0.3) & (MIPCHI2DV(PRIMARY) > 25) & (PIDK < 0)",
                                                  "K-"  : "(PT > 600*MeV) & (TRCHI2DOF < 5) & (TRGHOSTPROB < 0.3) & (MIPCHI2DV(PRIMARY) > 25) & (PIDK > 0)",
                                                  "pi0" : "(PT > 1000*MeV)" },
                                CombinationCut = "(in_range( 1400*MeV, AM, 2300*MeV))",
                                MotherCut = "(PT>4000*MeV) & (VFASPF(VCHI2PDOF) < 9) & (BPVVDCHI2 > 100) & (BPVIPCHI2() < 9) & (BPVDIRA > 0.9999) & (in_range(1600*MeV, M,2100*MeV))" )

#track filter + D0 combiner
MyD02Kpipi0M = CombineParticles("MyD02Kpipi0M",
                                DecayDescriptor = "[D0 -> K- pi+ pi0]cc",                            
                                Inputs = [ "Phys/StdLoosePions/Particles" ,"Phys/StdLooseKaons/Particles" , "Phys/StdLooseMergedPi0"],
                                DaughtersCuts = { "pi+" : "(PT > 300*MeV) & (TRCHI2DOF < 5) & (TRGHOSTPROB < 0.3) & (MIPCHI2DV(PRIMARY) > 25) & (PIDK < 0)",
                                                  "K-"  : "(PT > 300*MeV) & (TRCHI2DOF < 5) & (TRGHOSTPROB < 0.3) & (MIPCHI2DV(PRIMARY) > 25) & (PIDK > 0)",
                                                  "pi0" : "(PT > 2000*MeV)" },
                                CombinationCut = "(in_range( 1400*MeV, AM, 2300*MeV))",
                                MotherCut = "(PT>4000*MeV) & (VFASPF(VCHI2PDOF) < 9) & (BPVVDCHI2 > 100) & (BPVIPCHI2() < 9) & (BPVDIRA > 0.9999) & (in_range(1600*MeV, M,2100*MeV))" )

#D* -> D0 pi+
MyDst2D0piR = CombineParticles("MyDst2D0piR",
                               DecayDescriptor= "[ D*(2010)+ -> D0 pi+ ]cc",
                               Inputs = ["Phys/MyD02Kpipi0R" , "Phys/StdAllNoPIDsPions"],
                               DaughtersCuts = { "pi+"   : "(TRCHI2DOF < 5) & (TRGHOSTPROB < 0.3) & (MIPCHI2DV(PRIMARY) < 16)"},
                               CombinationCut = "(in_range( 95.421*MeV, (AM - AM1), 195.421*MeV))",
                               MotherCut = "(PT>4000*MeV) & (VFASPF(VCHI2PDOF) < 9) & (BPVVDCHI2 <16) & (BPVIPCHI2() < 9) & (in_range( 135.421*MeV, (M - M1), 155.421*MeV))")

#D* -> D0 pi+
MyDst2D0piM = CombineParticles("MyDst2D0piM",
                               DecayDescriptor= "[ D*(2010)+ -> D0 pi+ ]cc",
                               Inputs = ["Phys/MyD02Kpipi0M" , "Phys/StdAllNoPIDsPions"],
                               DaughtersCuts = { "pi+"   : "(TRCHI2DOF < 5) & (TRGHOSTPROB < 0.3) & (MIPCHI2DV(PRIMARY) < 16)"},
                               CombinationCut = "(in_range( 95.421*MeV, (AM - AM1), 195.421*MeV))",
                               MotherCut = "(PT>4000*MeV) & (VFASPF(VCHI2PDOF) < 9) & (BPVVDCHI2 <16) & (BPVIPCHI2() < 9) & (in_range( 135.421*MeV, (M - M1), 155.421*MeV))")

#Ntuple##################################################################
#Ntuples resolved#
MyDst2D0piR_Tuple = DecayTreeTuple("MyDst2D0piR_Tuple")
MyDst2D0piR_Tuple.Decay = "[D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^(pi0 -> ^gamma ^gamma)) ^pi+]CC"
MyDst2D0piR_Tuple.Inputs = [ "Phys/MyDst2D0piR/Particles" ]
MyDst2D0piR_Tuple.ToolList = ["TupleToolEventInfo","TupleToolRecoStats","TupleToolKinematic"]

MyDst2D0piR_Tuple.addBranches({
    "Dst_2010_plus"   : "[D*(2010)+]CC",
    "D0"              : "[D*(2010)+ -> ^(D0 -> K- pi+ (pi0 -> gamma gamma)) pi+]CC",
    "K_minus"         : "[D*(2010)+ -> (D0 -> ^K- pi+ (pi0 -> gamma gamma)) pi+]CC",
    "pi_plus"         : "[D*(2010)+ -> (D0 -> K- ^pi+ (pi0 -> gamma gamma)) pi+]CC",
    "pi0"             : "[D*(2010)+ -> (D0 -> K- pi+ ^(pi0 -> gamma gamma)) pi+]CC",
    "gamma0"          : "[D*(2010)+ -> (D0 -> K- pi+ (pi0 -> ^gamma gamma)) pi+]CC",
    "gamma"           : "[D*(2010)+ -> (D0 -> K- pi+ (pi0 -> gamma ^gamma)) pi+]CC",
    "pi_bach"         : "[D*(2010)+ -> (D0 -> K- pi+ (pi0 -> gamma gamma)) ^pi+]CC"})

MyDst2D0piR_Tuple.Dst_2010_plus.addTupleTool("TupleToolGeometry")
MyDst2D0piR_Tuple.Dst_2010_plus.addTupleTool("TupleToolVtxIsoln")
MyDst2D0piR_Tuple.D0.addTupleTool("TupleToolGeometry")

MyDst2D0piR_Tuple.K_minus.addTupleTool("TupleToolPid")
MyDst2D0piR_Tuple.pi_plus.addTupleTool("TupleToolPid")
MyDst2D0piR_Tuple.pi_bach.addTupleTool("TupleToolPid")

MyDst2D0piR_Tuple.pi0.addTupleTool("TupleToolConeIsolation")

MyDst2D0piR_Tuple.addTupleTool("TupleToolVeto")
MyDst2D0piR_Tuple.TupleToolVeto.Particle="pi0"
MyDst2D0piR_Tuple.TupleToolVeto.VetoOther["Pi0R"]=["/Event/Phys/StdLooseResolvedPi0/Particles"]
MyDst2D0piR_Tuple.TupleToolVeto.Veto["EtaR"]=["/Event/Phys/StdLooseResolvedEta/Particles"]

MyDst2D0piR_Tuple.gamma0.addTupleTool("TupleToolPhotonInfo")
MyDst2D0piR_Tuple.gamma0.addTupleTool("TupleToolProtoPData")
MyDst2D0piR_Tuple.gamma0.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID"]

MyDst2D0piR_Tuple.gamma0.addTupleTool("TupleToolCaloHypo")
MyDst2D0piR_Tuple.gamma0.TupleToolCaloHypo.DataList = ["HypoSpdM", "ToSpdM" , "HypoPrsM" , "ToPrsM" , "CaloHypoPileUpEnergy" ]

MyDst2D0piR_Tuple.gamma.addTupleTool("TupleToolPhotonInfo")
MyDst2D0piR_Tuple.gamma.addTupleTool("TupleToolProtoPData")
MyDst2D0piR_Tuple.gamma.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID"]

MyDst2D0piR_Tuple.gamma.addTupleTool("TupleToolCaloHypo")
MyDst2D0piR_Tuple.gamma.TupleToolCaloHypo.DataList = ["HypoSpdM", "ToSpdM" , "HypoPrsM" , "ToPrsM" , "CaloHypoPileUpEnergy" ]

LoKi_D0 = MyDst2D0piR_Tuple.D0.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_D0")
LoKi_D0.Preambulo = ["from LoKiCore.math import sqrt","Kpi_MM = sqrt(CHILD(MM,1)*CHILD(MM,1) + CHILD(MM,2)*CHILD(MM,2) + 2 *( CHILD(E,1)*CHILD(E,2) - (CHILD(PX,1)*CHILD(PX,2) + CHILD(PY,1)*CHILD(PY,2) + CHILD(PZ,1)*CHILD(PZ,2))))","pipi0_MM = sqrt(CHILD(MM,2)*CHILD(MM,2) + CHILD(MM,3)*CHILD(MM,3) + 2 *( CHILD(E,2)*CHILD(E,3) - (CHILD(PX,2)*CHILD(PX,3) + CHILD(PY,2)*CHILD(PY,3) + CHILD(PZ,2)*CHILD(PZ,3))))","Kpi0_MM = sqrt(CHILD(MM,1)*CHILD(MM,1) + CHILD(MM,3)*CHILD(MM,3) + 2 *( CHILD(E,1)*CHILD(E,3) - (CHILD(PX,1)*CHILD(PX,3) + CHILD(PY,1)*CHILD(PY,3) + CHILD(PZ,1)*CHILD(PZ,3))))"]
LoKi_D0.Variables = {"Kpi_MM" : "Kpi_MM" , "Kpi0_MM" : "Kpi0_MM" , "pipi0_MM" : "pipi0_MM"}

#Ntuples merged#
MyDst2D0piM_Tuple = DecayTreeTuple("MyDst2D0piM_Tuple")
MyDst2D0piM_Tuple.Decay = "[D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^pi0) ^pi+]CC"
MyDst2D0piM_Tuple.Inputs = [ "Phys/MyDst2D0piM/Particles" ]
MyDst2D0piM_Tuple.ToolList = ["TupleToolEventInfo","TupleToolRecoStats","TupleToolKinematic"]

MyDst2D0piM_Tuple.addBranches({
    "Dst_2010_plus"   : "[D*(2010)+]CC",
    "D0"              : "[D*(2010)+ -> ^(D0 -> K- pi+ pi0) pi+]CC",
    "K_minus"         : "[D*(2010)+ -> (D0 -> ^K- pi+ pi0) pi+]CC",
    "pi_plus"         : "[D*(2010)+ -> (D0 -> K- ^pi+ pi0) pi+]CC",
    "pi0"             : "[D*(2010)+ -> (D0 -> K- pi+ ^pi0) pi+]CC",
    "pi_bach"         : "[D*(2010)+ -> (D0 -> K- pi+ pi0) ^pi+]CC"})

MyDst2D0piM_Tuple.Dst_2010_plus.addTupleTool("TupleToolGeometry")
MyDst2D0piM_Tuple.Dst_2010_plus.addTupleTool("TupleToolVtxIsoln")
MyDst2D0piM_Tuple.D0.addTupleTool("TupleToolGeometry")

MyDst2D0piM_Tuple.K_minus.addTupleTool("TupleToolPid")
MyDst2D0piM_Tuple.pi_plus.addTupleTool("TupleToolPid")
MyDst2D0piM_Tuple.pi_bach.addTupleTool("TupleToolPid")

MyDst2D0piM_Tuple.pi0.addTupleTool("TupleToolConeIsolation")

MyDst2D0piM_Tuple.pi0.addTupleTool("TupleToolPi0Info")
MyDst2D0piM_Tuple.pi0.addTupleTool("TupleToolProtoPData")
MyDst2D0piM_Tuple.pi0.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID"]

MyDst2D0piM_Tuple.pi0.addTupleTool("TupleToolCaloHypo")
MyDst2D0piM_Tuple.pi0.TupleToolCaloHypo.DataList = ["HypoSpdM", "ToSpdM" , "HypoPrsM" , "ToPrsM" , "CaloHypoPileUpEnergy" ]

LoKi_D0 = MyDst2D0piM_Tuple.D0.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_D0")
LoKi_D0.Preambulo = ["from LoKiCore.math import sqrt","Kpi_MM = sqrt(CHILD(MM,1)*CHILD(MM,1) + CHILD(MM,2)*CHILD(MM,2) + 2 *( CHILD(E,1)*CHILD(E,2) - (CHILD(PX,1)*CHILD(PX,2) + CHILD(PY,1)*CHILD(PY,2) + CHILD(PZ,1)*CHILD(PZ,2))))","pipi0_MM = sqrt(CHILD(MM,2)*CHILD(MM,2) + CHILD(MM,3)*CHILD(MM,3) + 2 *( CHILD(E,2)*CHILD(E,3) - (CHILD(PX,2)*CHILD(PX,3) + CHILD(PY,2)*CHILD(PY,3) + CHILD(PZ,2)*CHILD(PZ,3))))","Kpi0_MM = sqrt(CHILD(MM,1)*CHILD(MM,1) + CHILD(MM,3)*CHILD(MM,3) + 2 *( CHILD(E,1)*CHILD(E,3) - (CHILD(PX,1)*CHILD(PX,3) + CHILD(PY,1)*CHILD(PY,3) + CHILD(PZ,1)*CHILD(PZ,3))))"]
LoKi_D0.Variables = {"Kpi_MM" : "Kpi_MM" , "Kpi0_MM" : "Kpi0_MM" , "pipi0_MM" : "pipi0_MM"}

#------------------------------------------------------------------







#-------------------- Hlt2 filters -------------------------------

filterHlt2_eta2mumugamma = LoKi__HDRFilter( 'MyHlt2Filter_Eta2MuMuGamma', Code="HLT_PASS('Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_kstrgamma = LoKi__HDRFilter( 'MyHlt2Filter_KstrGamma', Code="HLT_PASS('Hlt2CaloPIDBd2KstGammaTurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_phigamma  = LoKi__HDRFilter( 'MyHlt2Filter_PhiGamma', Code="HLT_PASS('Hlt2CaloPIDBs2PhiGammaTurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_ds2etappi = LoKi__HDRFilter( 'MyHlt2Filter_Ds2EtapPi', Code="HLT_PASS('Hlt2CaloPIDD2EtapPiTurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_dsstr2dsgamma = LoKi__HDRFilter( 'MyHlt2Filter_Dsstr2DsGamma', Code="HLT_PASS('Hlt2CaloPIDDsst2DsGammaTurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_resolved = LoKi__HDRFilter( 'MyHlt2Filter_ResolvedPi0', Code="HLT_PASS('Hlt2CaloPIDDstD02KPiPi0_ResolvedPi0TurboCalibDecision')", Location="Hlt2/DecReports" )
filterHlt2_merged = LoKi__HDRFilter( 'MyHlt2Filter_MergedPi0', Code="HLT_PASS('Hlt2CaloPIDDstD02KPiPi0_MergedPi0TurboCalibDecision')", Location="Hlt2/DecReports" )

#------------------------------------------------------------------




#-------------------- Sequences -----------------------------------

#Eta -> mu mu gamma
MySeq_Eta2MuMuGamma = GaudiSequencer("Phys/MySeq_Eta2MuMuGamma")
MySeq_Eta2MuMuGamma.Members += [filterHlt2_eta2mumugamma]
MySeq_Eta2MuMuGamma.Members += [MyEta2MuMuGamma]
MySeq_Eta2MuMuGamma.Members += [MyEta2MuMuGamma_Tuple]

#B0 -> K* gamma
MySeq_B02KstrGamma = GaudiSequencer("Phys/MySeq_B02KstrGamma")
MySeq_B02KstrGamma.Members += [filterHlt2_kstrgamma]
MySeq_B02KstrGamma.Members += [MyKstr2Kpi]
MySeq_B02KstrGamma.Members += [MyB02KstrGamma]
MySeq_B02KstrGamma.Members += [MyB02KstrGamma_Tuple]

#Bs -> Phi gamma
MySeq_Bs2PhiGamma = GaudiSequencer("Phys/MySeq_Bs2PhiGamma")
MySeq_Bs2PhiGamma.Members += [filterHlt2_phigamma]
MySeq_Bs2PhiGamma.Members += [MyPhi2KK]
MySeq_Bs2PhiGamma.Members += [MyBs2PhiGamma]
MySeq_Bs2PhiGamma.Members += [MyBs2PhiGamma_Tuple]

#D+ -> eta' pi+
MySeq_Ds2EtapPi = GaudiSequencer('MySeq_Ds2EtapPi')
MySeq_Ds2EtapPi.Members += [filterHlt2_ds2etappi]
MySeq_Ds2EtapPi.Members += [MyEtap2PiPiGamma]
MySeq_Ds2EtapPi.Members += [MyDs2EtapPi]
MySeq_Ds2EtapPi.Members += [MyDs2EtapPi_Tuple]

#Ds* -> Ds gamma
MySeq_Dsstr2DsGamma = GaudiSequencer("Phys/MySeq_Dsstr2DsGamma")
MySeq_Dsstr2DsGamma.Members += [filterHlt2_dsstr2dsgamma]
MySeq_Dsstr2DsGamma.Members += [Ds2KKpi_Seq.sequence()]
MySeq_Dsstr2DsGamma.Members += [MyDsstr2DsGamma]
MySeq_Dsstr2DsGamma.Members += [MyDsstr2DsGamma_Tuple]

#D0 -> K pi pi0R
MySeq_Dst2D0piR = GaudiSequencer("Phys/MySeq_Dst2D0piR")
MySeq_Dst2D0piR.Members += [filterHlt2_resolved]
MySeq_Dst2D0piR.Members += [MyD02Kpipi0R]
MySeq_Dst2D0piR.Members += [MyDst2D0piR]
MySeq_Dst2D0piR.Members += [MyDst2D0piR_Tuple]

#D0 -> K pi pi0M
MySeq_Dst2D0piM = GaudiSequencer("Phys/MySeq_Dst2D0piM")
MySeq_Dst2D0piM.Members += [filterHlt2_merged]
MySeq_Dst2D0piM.Members += [MyD02Kpipi0M]
MySeq_Dst2D0piM.Members += [MyDst2D0piM]
MySeq_Dst2D0piM.Members += [MyDst2D0piM_Tuple]

DaVinci().UserAlgorithms = [ MySeq_Eta2MuMuGamma ,
                             MySeq_B02KstrGamma ,
                             MySeq_Bs2PhiGamma ,
                             MySeq_Ds2EtapPi ,
                             MySeq_Dsstr2DsGamma ,
                             MySeq_Dst2D0piR ,
                             MySeq_Dst2D0piM ]
