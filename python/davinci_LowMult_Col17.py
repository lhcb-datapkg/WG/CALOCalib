import os
os.environ['NO_GIT_CONDDB']='1'
#os.environ['SQLITEDBPATH']='/afs/cern.ch/lhcb/software/DEV/DBASE/Det/SQLDDDB/db'

from Gaudi.Configuration import *
from Configurables import DaVinci
import GaudiKernel.SystemOfUnits as Units
from GaudiConfUtils import ConfigurableGenerators
from Configurables import FilterDesktop, CombineParticles
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from DecayTreeTuple.Configuration import *
from Configurables import LoKi__Hybrid__TupleTool, TupleToolDecay
from Configurables import LoKi__HDRFilter
from Configurables import GaudiSequencer, PrintDecayTree


#from Configurables import PhysConf
#PhysConf().CaloReProcessing=True
#from Configurables import CondDB
#conddb = CondDB()
#conddb.addLayer(dbFile = "LHCBCOND-Oct2017-Unconv.db", dbName = "LHCBCOND")


DaVinci().Simulation = 0
DaVinci().EvtMax = 1000
DaVinci().DataType = "2017"
DaVinci().TupleFile = "Ntuple_LMP_2017.root"
DaVinci().InputType = "DST"
DaVinci().Lumi = True


#LowMult stream - 2017
DaVinci().Input = ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/FULL.DST/00064144/0000/00064144_00003146_1.full.dst"]





############# pi0 -> gamma gamma #########################################
MyPi0R = CombineParticles("MyPi0R",
                          DecayDescriptor= "pi0 -> gamma gamma",
                          Inputs = ["Phys/StdLooseAllPhotons/Particles"],
                          DaughtersCuts = { "gamma" : " (CL>0.05) "},
                          CombinationCut = "ATRUE",
                          MotherCut="(MM > 80*MeV) & (MM < 200*MeV) & (PT>1000*MeV)")
MyPi0R.ParticleCombiners.update( { "" : "ParticleAdder"} )

#Tuple
MyPi0R_Tuple = DecayTreeTuple("MyPi0R_Tuple")
MyPi0R_Tuple.Decay = "[pi0 -> ^gamma ^gamma]CC"
MyPi0R_Tuple.Inputs = [ "Phys/MyPi0R" ]
MyPi0R_Tuple.ToolList = ["TupleToolEventInfo","TupleToolRecoStats","TupleToolPrimaries","TupleToolKinematic"]

MyPi0R_Tuple.addBranches({
    "pi0"       : "[pi0]CC",
    "gamma0"   : "[pi0 -> ^gamma gamma]CC",
    "gamma"    : "[pi0 -> gamma ^gamma]CC"})

MyPi0R_Tuple.pi0.addTupleTool("TupleToolConeIsolation")

MyPi0R_Tuple.gamma0.addTupleTool("TupleToolPhotonInfo")
MyPi0R_Tuple.gamma0.addTupleTool("TupleToolProtoPData")
MyPi0R_Tuple.gamma0.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID"]
MyPi0R_Tuple.gamma0.addTupleTool("TupleToolCaloHypo")
MyPi0R_Tuple.gamma0.TupleToolCaloHypo.DataList = ["HypoSpdM", "ToSpdM" , "HypoPrsM" , "ToPrsM" ]

MyPi0R_Tuple.gamma.addTupleTool("TupleToolPhotonInfo")
MyPi0R_Tuple.gamma.addTupleTool("TupleToolProtoPData")
MyPi0R_Tuple.gamma.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID"]
MyPi0R_Tuple.gamma.addTupleTool("TupleToolCaloHypo")
MyPi0R_Tuple.gamma.TupleToolCaloHypo.DataList = ["HypoSpdM", "ToSpdM" , "HypoPrsM" , "ToPrsM" ]


############# eta -> gamma gamma #########################################
MyEtaR = CombineParticles("MyEtaR",
                          DecayDescriptor= "eta -> gamma gamma",
                          Inputs = ["Phys/StdLooseAllPhotons/Particles"],
                          DaughtersCuts = { "gamma" : " (CL>0.05) "},
                          CombinationCut = "ATRUE",
                          MotherCut="(MM > 400*MeV) & (MM < 700*MeV) & (PT>1000*MeV)")
MyEtaR.ParticleCombiners.update( { "" : "ParticleAdder"} )

#Tuple
MyEtaR_Tuple = DecayTreeTuple("MyEtaR_Tuple")
MyEtaR_Tuple.Decay = "[eta -> ^gamma ^gamma]CC"
MyEtaR_Tuple.Inputs = [ "Phys/MyEtaR" ]
MyEtaR_Tuple.ToolList = ["TupleToolEventInfo","TupleToolRecoStats","TupleToolPrimaries","TupleToolKinematic"]

MyEtaR_Tuple.addBranches({
    "eta"       : "[eta]CC",
    "gamma0"   : "[eta -> ^gamma gamma]CC",
    "gamma"    : "[eta -> gamma ^gamma]CC"})

MyEtaR_Tuple.eta.addTupleTool("TupleToolConeIsolation")

MyEtaR_Tuple.gamma0.addTupleTool("TupleToolPhotonInfo")
MyEtaR_Tuple.gamma0.addTupleTool("TupleToolProtoPData")
MyEtaR_Tuple.gamma0.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID"]

MyEtaR_Tuple.gamma.addTupleTool("TupleToolPhotonInfo")
MyEtaR_Tuple.gamma.addTupleTool("TupleToolProtoPData")
MyEtaR_Tuple.gamma.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID"]



############# Merged pi0 -> gamma gamma ###################################
from Configurables import MergedPi0Maker
from GaudiKernel.SystemOfUnits import MeV
algorithmM =  MergedPi0Maker ( 'Phys/StdLooseMergedPi0',  DecayDescriptor = 'Pi0', MassWindow = 65.* MeV )

#Tuple
MyPi0M_Tuple = DecayTreeTuple("MyPi0M_Tuple")
MyPi0M_Tuple.Decay = "[pi0]CC"
MyPi0M_Tuple.Inputs = [ "Phys/StdLooseMergedPi0" ]

MyPi0M_Tuple.ToolList = ["TupleToolEventInfo","TupleToolRecoStats","TupleToolPrimaries","TupleToolKinematic"]

MyPi0M_Tuple.addBranches({"pi0" : "[pi0]CC"})

MyPi0M_Tuple.pi0.addTupleTool("TupleToolConeIsolation")

MyPi0M_Tuple.pi0.addTupleTool("TupleToolPi0Info")
MyPi0M_Tuple.pi0.addTupleTool("TupleToolProtoPData")
MyPi0M_Tuple.pi0.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID"]








######### Sequence ###################################

#Pi0R
MySeq_Pi0R = GaudiSequencer("Phys/MySeq_Pi0R")
MySeq_Pi0R.Members += [MyPi0R]
MySeq_Pi0R.Members += [MyPi0R_Tuple]

#EtaR
MySeq_EtaR = GaudiSequencer("Phys/MySeq_EtaR")
MySeq_EtaR.Members += [MyEtaR]
MySeq_EtaR.Members += [MyEtaR_Tuple]

#Pi0M
MySeq_Pi0M = GaudiSequencer("Phys/MySeq_Pi0M")
MySeq_Pi0M.Members += [algorithmM]
MySeq_Pi0M.Members += [MyPi0M_Tuple]


DaVinci().UserAlgorithms = [ MySeq_Pi0R , MySeq_EtaR , MySeq_Pi0M ]
